import 'package:newone/model/check_card.dart';

class CardInfo {
  String detail;
  DateTime actualTime;
  DateTime estimateTime;
  int featurePoint;
  List<CheckCard> checkList;

  CardInfo(
      {this.detail,
      this.actualTime,
      this.estimateTime,
      this.featurePoint,
      this.checkList});
}
