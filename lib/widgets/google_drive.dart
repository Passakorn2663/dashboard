import 'package:flutter/material.dart';

class GoogleDrive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Icon(
        Icons.drive_file_move_outline,
        size: 20,
        semanticLabel: 'Google Drive',
      ),
      onPressed: () => null,
    );
  }
}
