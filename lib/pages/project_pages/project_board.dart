import 'package:flutter/material.dart';
import 'package:newone/widgets/google_drive.dart';
import 'package:newone/widgets/search_box.dart';

class ProjectBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Sleep More'),
        ),
        body: Column(
          children: [
            Text('Project1'),
            Center(
              child: Row(
                children: [
                  Container(
                    child: SearchBox(),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                  Container(
                    child: Row(
                      children: [
                        RaisedButton(
                          onPressed: () => null,
                          child: Text('OK'),
                        ),
                        RaisedButton(
                          onPressed: () => null,
                          child: Text('OK'),
                        ),
                        RaisedButton(
                          onPressed: () => null,
                          child: Text('OK'),
                        ),
                        GoogleDrive(),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
